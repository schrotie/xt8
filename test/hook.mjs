import {
	create,
	hookDel,
	unhookDel,
	hookGet,
	unhookGet,
	hookSet,
	unhookSet,
} from '../index.mjs';

describe('hook', function() {
	let state;
	beforeEach(function() {
		state = create({});
	});

	describe('get', function() {
		afterEach(function() {unhookGet('foo', get1);});
		function get1() {return 'test1';}
		function get2() {return 'test2';}
		it('should return value from hook', function() {
			hookGet('foo', get1);
			state.foo.should.equal('test1');
		});

		it('should return undefined', function() {
			(state.foo === undefined).should.be.true;
			hookGet('foo', get1);
		});

		it('should get property descriptor', function() {
			hookGet('foo', get1);
			Object.getOwnPropertyDescriptor(state, 'foo')
				.get.should.be.a('function');
		});


		it('should throw not-registered hook', function() {
			(() => unhookGet('foo', get2)).should
				.throw('Cannot unhook "foo" - hook not found');
			hookGet('foo', get1);
			(() => unhookGet('foo', get2)).should
				.throw('Cannot unhook "foo" - hook not found');
		});
	});

	describe('get deep', function() {
		// This throws; TODO: bug?
		// after(function() {
		// 	unhookGet('foo.bar', get);
		// });
		function get() {return 'test';}
		it('should return value from deep hook', function() {
			state = create({foo: {}});
			hookGet('foo.bar', get);
			state.foo.bar.should.equal('test');
		});

		it('should list keys from deep hook', function() {
			state = create({foo: {}});
			Object.keys(state.foo).should.deep.equal(['bar']);
		});
	});

	describe('set', function() {
		let called;
		beforeEach(function() {called = false;});
		afterEach(function() {unhookSet('foo', set);});
		function set() {return called = true;}
		it('should call setter', function() {
			hookSet('foo', set);
			state.foo = 1;
			called.should.be.true;
		});
	});

	describe('del', function() {
		let called;
		beforeEach(function() {called = false;});
		afterEach(function() {unhookDel('foo', del);});
		function del() {return called = true;}
		it('should call setter', function() {
			hookDel('foo', del);
			delete state.foo;
			called.should.be.true;
		});
	});
});
